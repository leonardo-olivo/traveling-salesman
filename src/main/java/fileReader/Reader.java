package fileReader;

import lombok.NoArgsConstructor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.stream.Stream;

@NoArgsConstructor
public class Reader {

  public Stream<String> getCoordinates() {
    try {
      FileReader fileReader = new FileReader(getClass().getClassLoader().getResource("data.txt").getFile());
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      return bufferedReader.lines();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }
}
