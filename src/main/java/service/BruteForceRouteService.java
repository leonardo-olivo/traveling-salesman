package service;

import domain.City;
import domain.Route;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class BruteForceRouteService {

  private final List<List<Integer>> routes = new ArrayList<>();

  /**
   * Brute Forces all possible routes to pass every city
   * IMPORTANT the first entry in the List is always the start point
   *
   * @param cities list of city to pass through
   * @return All possible ways to go through all cities (ints are the unique IDs of the cities)
   */
  public List<Route> bruteForceRoute(List<City> cities) {

    // creates a linked list with the ids of the cities
    LinkedList<Integer> list = cities.stream().map(City::getId).collect(Collectors.toCollection(LinkedList::new));
    route(list, new LinkedList<>());

    // creates out of the ints an actual Route object
    return routes.stream().map(route -> Route.builder().route(route).build()).collect(Collectors.toList());
  }

  /**
   * Iterates through all elements and creates unique routes (accelerates with each entry)
   * I chose LinkedLists since it allows me to remove the first element (Not like ArrayList or default List)
   * First Element is always added to the new rotation which in my cases works since it's my start point
   * Route doesn't add the return route but it could be added easily. (I don't do that since my total distance calculator is able to handle that)
   *
   * @param ids    list of all remaining ids that haven't found a place in the new route
   * @param before history of added elements (in this cycle)
   */
  private void route(List<Integer> ids, List<Integer> before) {
    // creates a copy since the reference is still active and would eventually result into an empty list
    LinkedList<Integer> copy = new LinkedList<>(List.copyOf(ids));

    // adds the first element to the before/history array which includes all previous entries
    before.add(copy.get(0));
    // removes the first element since we don't want it duplicated in the next recursive call
    copy.remove(0);

    // checks if the element is empty and if it is, that means that a new complete route was generated
    if (copy.size() == 0) {
      // add the new route to the List and end the recursive rotation
      routes.add(before);
    } else {
      // if there are still elements left we need to iterate through them
      for (int i = 0; i < copy.size(); i++) {
        // recursive call with the remaining entries (Important! New reference!)
        route(copy, new LinkedList<>(List.copyOf(before)));
        // this creates a new order of the remaining entries so we get all possible orders
        // gets called when a new list was generated and the order needs to be shifted so a new order can be generated
        copy = moveBack(copy);
      }
    }
  }


  /**
   * Moves each entry one back and the first element to the last position
   *
   * @param toMove list to move
   * @return moved list
   */
  private LinkedList<Integer> moveBack(LinkedList<Integer> toMove) {
    LinkedList<Integer> moved = new LinkedList<>();
    Integer toKeep = toMove.get(0);

    for (int i = 1; i < toMove.size(); i++) {
      moved.add(toMove.get(i));
    }

    moved.add(toKeep);
    return moved;
  }
}
