import domain.City;
import domain.Distance;
import domain.Route;
import parser.CoordinatesParser;
import scanner.Interact;
import service.BruteForceRouteService;
import util.DistanceCalculatorUtil;
import util.RouteCalculatorUtil;

import java.util.List;
import java.util.stream.Collectors;

public class Main {

  // The main method is only to get the data, call methods, pass methods the data which we got and print out the results
  public static void main(String[] args) {
    CoordinatesParser coordinatesParser = new CoordinatesParser();

    List<City> cities = DistanceCalculatorUtil.calculate(coordinatesParser.parseCoordinates());

    System.out.println("Print Distances");
    printDistances(cities);

    System.out.println("");
    System.out.println("Shortest route from one city");
    shortestPoint(cities);

    System.out.println("");
    System.out.println("Brute force algorithm");
    bruteForce(cities);
  }

  private static void bruteForce(List<City> cities) {
    // check for amount of cities and if there are more than 9, ask if the user wants to run the brute force algorithm or not
    if (cities.size() > 9 && !Interact.askBruteForce(cities.size())) {
      return;
    }

    BruteForceRouteService bruteForceRouteService = new BruteForceRouteService();

    List<Route> bruteForced = bruteForceRouteService.bruteForceRoute(cities);

    System.out.println("All possible routes");

    for (Route route: bruteForced) {
      System.out.print("{ ");
      for (int id: route.getRoute()) {
        System.out.print(id + ", ");
      }
      System.out.print("0 }");
      System.out.println("");
    }

    System.out.println("Total amount of routes: " + bruteForced.size());

    calculateShortestBruteForce(bruteForced, cities);
  }

  private static void calculateShortestBruteForce(List<Route> bruteForcedRoutes, List<City> cities) {
    bruteForcedRoutes.forEach(route -> {
      route.setTotalDistance(DistanceCalculatorUtil.distanceDriven(route.getRoute(), cities));
    });

    bruteForcedRoutes = bruteForcedRoutes.stream().sorted().collect(Collectors.toList());

    System.out.println("");
    System.out.println("Following routes are the fastest according to the brute forcing: ");

    for (int i = 0; i < 3; i++) {

      // check if that many routes exist
      if (bruteForcedRoutes.size() - 1 > i) {
        Route route = bruteForcedRoutes.get(i);

        System.out.print("{ ");

        for (Integer pos: route.getRoute()) {
          System.out.print(pos + ", ");
        }

        System.out.println("0 }");
        System.out.println("Total distance: " + route.getTotalDistance());
      }
    }
  }

  private static void shortestPoint(List<City> cities) {
    Route route = RouteCalculatorUtil.route(cities);
    route.setTotalDistance(DistanceCalculatorUtil.distanceDriven(route.getRoute(), cities));
    List<Integer> routeIds = route.getRoute();

    System.out.print("Route: ");
    System.out.println("{ " + routeIds.stream().map(Object::toString).collect(Collectors.joining(", ")) + " }");
    System.out.println("Total distance: " + route.getTotalDistance());
  }

  private static void printDistances(List<City> cities) {
    for (City city: cities) {
      for (Distance coordinate: city.getDistances()) {
        System.out.print("{ id " + coordinate.getCityId() + " distance " + coordinate.getDistance() + "}, ");
      }
      System.out.println("");
    }

  }
}
