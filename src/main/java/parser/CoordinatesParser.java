package parser;

import domain.Coordinate;
import fileReader.Reader;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public class CoordinatesParser {

  /**
   * Reads the data from the data file and parses them into a {@link Coordinate} object
   * @return a list of Coordinates
   */
  public List<Coordinate> parseCoordinates() {
    Reader reader = new Reader();
    List<String> stringData = reader.getCoordinates().collect(Collectors.toList());
    List<Coordinate> coordinates = new ArrayList<>();

    for (int i = 0; i < stringData.size(); i++) {
      coordinates.add(parse(stringData.get(i), i));
    }

    return coordinates;
  }

  /**
   * Creates a new Coordinate object with the input that it got and the key
   * @param input x and y
   * @param key id of the coordinate
   * @return a new coordinate
   */
  private Coordinate parse(String input, Integer key) {
    List<String> data = Arrays.asList(input.split("\t").clone());

    return new Coordinate(key, Integer.parseInt(data.get(0)), Integer.parseInt(data.get(1)));
  }
}
