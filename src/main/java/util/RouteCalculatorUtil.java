package util;

import domain.City;
import domain.Distance;
import domain.Route;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class RouteCalculatorUtil {

  private RouteCalculatorUtil() {}

  /**
   * Calculates the route according to the closest point from the current position and which wasn't passed already
   * @param cities to go through
   * @return a route of IDs from the cities
   */
  public static Route route(List<City> cities) {
    // current position basically means the location which the car is right now
    int pos = 0;
    List<Integer> route = new ArrayList<>();
    List<Distance> distancesFromOneCity;

    // starting position is already passed
    route.add(0);

    for (int i = 0; i < cities.size(); i++) {
      // gets all distances from the current position
      distancesFromOneCity = List.copyOf(cities.get(pos).getDistances());

      // sorts the distances according to their length (shortest first)
      distancesFromOneCity = distancesFromOneCity.stream().sorted().collect(Collectors.toList());

      // boolean to test if we passed this city already
      boolean notPassedCityFound = false;

      // we need to check for the shortest distance if we passed the city already and if we did we need to go
      // and get the second shortest and check that and so on
      for (int y = 1; y < distancesFromOneCity.size() && !notPassedCityFound; y++) {
        int index = distancesFromOneCity.get(y).getCityId();

        // check if we have it
        if (!route.contains(index)) {
          // if not, we drive to this city and mark it as our new current position
          pos = index;
          notPassedCityFound = true;
          // add it to our route
          route.add(index);
        }
      }
    }

    // distance will be calculated later on
    return new Route(route, null);
  }
}
