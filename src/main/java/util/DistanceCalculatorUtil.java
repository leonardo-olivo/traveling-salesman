package util;

import domain.City;
import domain.Coordinate;
import domain.Distance;

import java.util.ArrayList;
import java.util.List;

public final class DistanceCalculatorUtil {

  private DistanceCalculatorUtil() {
  }

  /**
   * Creates a List of Cities from the coordinates it got. The cities contain all the distances to all other points
   *
   * @param coordinates data to calculate
   * @return list of the calculated data
   */
  public static List<City> calculate(List<Coordinate> coordinates) {

    List<City> distances = new ArrayList<>();
    for (int i = 0; i < coordinates.size(); i++) {
      distances.add(calculateFromOneCoordinate(coordinates.get(i), coordinates));
    }

    return distances;
  }

  /**
   * Calculates for a {@link Coordinate} the distance to all other points
   *
   * @param coordinate  point for which the distances are calculated
   * @param coordinates all coordinates to calculate each distance
   * @return a {@link City} with all distances and the id
   */
  private static City calculateFromOneCoordinate(Coordinate coordinate, List<Coordinate> coordinates) {
    City city = new City();
    List<Distance> distances = new ArrayList<>();

    for (Coordinate two: coordinates) {
      distances.add(calculateDistanceBetweenTwoPoints(coordinate, two));
    }

    city.setDistances(distances);
    city.setId(coordinate.getId());
    return city;
  }

  /**
   * Calculates a {@link Distance} from one to another coordinate
   *
   * @param one first coordinate
   * @param two second coordinate
   * @return a new Distance with the distance as long (not float since we don't need that much of an accuracy)
   * and the id of the second coordinate so we know to where this distance goes
   */
  private static Distance calculateDistanceBetweenTwoPoints(Coordinate one, Coordinate two) {
    int distanceX = Math.abs(one.getX() - two.getX());
    int distanceY = Math.abs(one.getY() - two.getY());

    // Calculate the Pythagoras between the points (rounded to ints)
    return new Distance(((int) Math.sqrt(Math.pow(distanceX, 2) + Math.pow(distanceY, 2))), two.getId());
  }

  /**
   * Calculates the distance according to the route and distance in the city list
   *
   * @param route  that the car takes/should take
   * @param cities all the cities with the distances needed to calculate the total distance
   * @return total distance driven when taking the route in the list
   */
  public static Integer distanceDriven(List<Integer> route, List<City> cities) {
    Integer totalDistance = 0;

    for (int currentPositionIndex = 0; currentPositionIndex < route.size(); currentPositionIndex++) {

      // when we are at the last position we want to go back so therefore we need to check that
      // and then either go to position 0 or not the next one planned in the route
      int nextPosition = route.size() - 1 == currentPositionIndex ? 0 : route.get(currentPositionIndex + 1);
      int currentPosition = route.get(currentPositionIndex);

      totalDistance += cities.get(currentPosition).getDistances().get(nextPosition).getDistance();
    }

    return totalDistance;
  }
}
