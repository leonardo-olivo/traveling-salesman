package scanner;

import java.util.Scanner;

public final class Interact {

   private Interact() {
   }

   public static boolean askBruteForce(Integer amount) {
      Scanner scanner = new Scanner(System.in);

      // a few spaces
      System.out.println("");
      System.out.println("");
      System.out.println("Are you sure that you want proceed with " + amount + " cities?");
      System.out.println("The process will take a long time.");
      System.out.println("Type yes to proceed with the brute force algorithm.");
      String answer = scanner.nextLine();

      return answer.equalsIgnoreCase("yes");
   }
}
