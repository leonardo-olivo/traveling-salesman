package domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Route implements Comparable<Route> {
  private List<Integer> route;
  private Integer totalDistance;

  @Override
  public int compareTo(Route o) {
    if (o.getTotalDistance() > this.getTotalDistance()) {
      return -1;
    } else if (o.getTotalDistance() < this.getTotalDistance()) {
      return 1;
    }

    return 0;
  }
}
