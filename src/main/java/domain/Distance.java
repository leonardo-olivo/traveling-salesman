package domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Distance implements Comparable<Distance> {
  private Integer distance;
  private Integer cityId;

  @Override
  public int compareTo(Distance o) {
    if (this.distance > o.getDistance()) {
      return 1;
    } else if (this.distance < o.getDistance()) {
      return -1;
    }

    return 0;
  }
}
