# Traveling Salesman
@ Leonardo Olivo

This program calculates the shortest route to pass all the cities and return to the origin city.
For that I included two algorithms.

The first one is just going to the closest, not yet visited city.
This doesn't necessarily return the best route but in most cases it should be on of the shorter ones.

The second algorithm is brute forcing all routes and returns the shortest route.
It is guaranteed to return the shortest route but since the possibilities exponentially grow with each city I don't recommend using this algorithm with more than 9 cities.

## Run the program
This is fairly easy since this project is a plain maven project. All that needs to be done is to run the Main method.


### Add Cities
The amount of cities are defined in the data.txt file which can be found in the resource folder.
Each city has a x and y coordinate. To add a city type a number for each the x and y coordinate. (The first being the x and the second being y)
Make sure to have a Tab between the numbers since the parser splits according to tabs.
The easiest and safest way to do it, is by copy pasting the line before and changing the numbers.


### Brute Force
Since the brute force algorithm is not that easy to understand I made program sequence with the cities 0, 1 and 2
The code can be found in BruteForceService.java line 41.

iteration 1 start: before = [], ids = [0, 1, 2]

add 0 to before and remove it from ids

before = [0], ids = [1, 2]

go into for loop for two rotation since 2 elements are left

create new recursive call with before = [0], ids = [1, 2]

iteration 2 start: before2 = [0], ids2 = [1, 2]

add 1 to before2 and remove it from ids2

before2 = [0, 1], ids2 = [2]

go into for loop since one element is still left

create new recursive call with before2 = [0, 1], ids2 = [2]

iteration 3: before3 = [0, 1], ids3 = [2]

add last element to before3 and create new route in routes

end iteration 3

go back to iteration 2

moveBack doesn't do anything go back one more iteration

back in iteration 1

move back leaves us with before = [0] and ids = [2, 1]

create new recursive call with before = [0] and ids = [2, 1]

iteration 4: before4 = [0] and ids4 = [2, 1]

remove 2 from ids4 and add it to before4

before4 = [0, 2] and ids4 = [1]

go into for loop since one element is still left

create new recursive call with before4 = [0, 2], ids4 = [1]

iteration 5: before5 = [0, 2], ids5 = [1]

add last element to before5 and create new route in routes

end iteration 5

elements left end iteration 4

no elements left end iteration 1

leaves us with following routes

[0, 1, 2] and [0, 2, 1]

0 is the start point
